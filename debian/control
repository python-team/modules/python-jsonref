Source: python-jsonref
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Paolo Greppi <paolo.greppi@libpf.com>
Build-Depends:
 debhelper-compat (= 10)
 , dh-python
 , python3-all
 , python3-setuptools
 , python3-markdown
 , python3-setuptools
 , python3-sphinx
 , python3-termcolor
 , python3-xmltodict
 , python3-yaml
Standards-Version: 3.9.8
Homepage: https://github.com/gazpachoking/jsonref
Vcs-Git: https://salsa.debian.org/python-team/packages/python-jsonref.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-jsonref
Testsuite: autopkgtest-pkg-python

Package: python3-jsonref
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-jsonref-doc
Description: JSON Reference implementation for Python 3
 Python library for automatic dereferencing of JSON Reference object.
 .
 It lets you use a data structure with JSON reference objects, as if the
 references had been replaced with the referent data. References are evaluated
 lazily. Nothing is dereferenced until it is used. Recursive references are
 supported, and create recursive Python data structures.
 .
 This package installs the library for Python 3.

Package: python-jsonref-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: JSON Reference implementation for Python (common documentation)
 Python library for automatic dereferencing of JSON Reference objects.
 .
 It lets you use a data structure with JSON reference objects, as if the
 references had been replaced with the referent data. References are evaluated
 lazily. Nothing is dereferenced until it is used. Recursive references are
 supported, and create recursive Python data structures.
 .
 This is the common documentation package.
